# alaiuri.org.ro docs
Hello, friend. This repository contains the technical documentation of alaiuri.org.ro project.

The project revolves around building a custom solution, because neither of existing ones meets our demands:
- simple to maintain;
- proper security by using Markdown-based content;
- fast enough for cheap hosting;
- embeddable into a future mobile app.

## Components and sub-projects
- main website: alaiuri.org.ro
- redaction panel: editor.alaiuri.org.ro
- API: api.alaiuri.org.ro

The API project has a low priority, since there are some things to consider first.

## Tech stack

This section covers the proposed tech stack and the reasoning of it per project basis. A more general reaasoning is covered in this section alone.

The proposed tech stack revolves around three main keys: simplicity, easiness and correctness.

## Project layers

There are some layers around the project:
1. Infrastructure layer: platform hosting, source code project;
2. Data layer: we're using SurrealDB to store the data;
3. Services layer: redactor platform, API;
4. User-facing layer: main website.

### Main website: alaiuri.org.ro

The website is prototyped in Nuxt 3 framework (which is a Vue 3 based web framework, revolving around NodeJS ecosystem). It does it's job pretty well. The main reasoning behind this choice is that Nuxt 3 has a Content module, which allows to write Markdown files that gets rendered, thus allowing us to "port" over any Markdown-based content we've written so far.

The main downside is, each new page **MUST** be pushed on the repository itself. While this is not *that* bad for a casual developer, for non-devs is a pain, due to necessity to learn Git, have a Github account, learn Content-specific Markdown syntax. Thus, we've decided to make the Redaction Panel: a simple platform where these downsides are fixed. However, this contradicts with the decision to use Nuxt as a whole, opening up the possibility to use any other frameworks.

For now, using Nuxt 3 is a good choice and the decision will be changed upon the next website's redesign.

### Redaction Panel

This panel is a simple interface with three main features:
- authentication;
- article management;
- user management.

On this one, we might still use Nuxt, since the main website is also made with Nuxt.

#### Authentication

This is the tricky part. The main logic would be to use Discord's Oauth2 flow to check if a user is on 00 and it has a special role. If everything checks, the user has access to the interface. Otherwise, a generic error message should be visible so that noone can hack us that easy.

#### Article management

This is the simple part. These are some security rules:
1. Each article **MUST** be saved in Markdown format;
2. Each new article **MUST** be saved as draft and synced periodically;
3. Each article **MUST** be approved by at least one `Redactor` for publishing;

Therefore, there are some operations that happens behind the scenes:
1. Create a new article;
2. Update an existing article;
3. Publish / Unpublish an article;
4. Approve an article;
5. Delete an article;

Any user can contribute to an article, so there's no single author. However, an article should have two fields:
- `author` -> the person who came up with the idea;
- `contributors` -> persons who contributed / edited the article.

#### User management

User management is pretty simple. There are two user roles:
- Redactor;
- Contributor;

The `Contributor` role is reserved for future usages, where we might allow community members to write an article for the website. But that's open to talks.

There are some user settings to take into account:
1. (optional; default = "auto") Theme preference: dark, light or auto;
2. (optional; default = false) Whether to display user's name on articles or not;
3. (optional; default = null) User's display name to be shown on the website.

### The API

The API is an optional project that will come in handy in the future. For now, it's not required nor decided upon it. However, it could be written in Rust and / or TypeScript + Deno.

Currently, there's no API shape whatsoever defined or layed out.
